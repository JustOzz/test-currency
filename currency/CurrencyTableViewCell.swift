//
//  CurrencyTableViewCell.swift
//  currency
//
//  Created by MacBook on 18.05.2018.
//  Copyright © 2018 justozz. All rights reserved.
//

import UIKit

class CurrencyTableViewCell: UITableViewCell {
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var amountLabel: UILabel!
    @IBOutlet weak var volumeLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    public func setData(data: Currency) {
        nameLabel.text = data.name
        amountLabel.text = String(format: "Amount: %.2f", data.price.amount)
        volumeLabel.text = String(format: "Volume: %d", data.volume)
    }
}
