//
//  ApiCurrencyService.swift
//  currency
//
//  Created by MacBook on 17.05.2018.
//  Copyright © 2018 justozz. All rights reserved.
//

import Foundation

protocol ApiCurrenciesServiceDelegate {
    func currenciesDidUpdate(_ currencies: [Currency]?)
}

class ApiCurrenciesService: NSObject {
    fileprivate static let API_URL = "http://phisix-api3.appspot.com/stocks.json"
    
    //Singleton
    public static let sharedInstance = ApiCurrenciesService()
    //
    
    fileprivate var refreshTimer: DispatchSourceTimer?
    
    open var delegate: ApiCurrenciesServiceDelegate? {
        didSet {
            self.restartUpdates()
        }
    }
    
    public func getCurrencies() {
        guard self.delegate != nil else {
            return
        }
        
        guard let url = URL(string: ApiCurrenciesService.API_URL) else {
            self.delegate?.currenciesDidUpdate(nil)
            return
        }
        
        URLSession.shared.dataTask(with: url) { (data, response, error) in
            guard let data = data else {
                self.delegate?.currenciesDidUpdate(nil)
                return
            }
            
            do {
                let currenciesResponse = try JSONDecoder().decode(CurrenciesResponse.self, from: data)
                self.delegate?.currenciesDidUpdate(currenciesResponse.stock)
            } catch {
                self.delegate?.currenciesDidUpdate(nil)
            }
        }.resume()
    }
    
    public func restartUpdates() {
        let queue = DispatchQueue(label: "com.justozz.currency.apicurrencysevice.timer", attributes: .concurrent)
        self.refreshTimer?.cancel()
        
        self.refreshTimer = DispatchSource.makeTimerSource(queue: queue)
        self.refreshTimer?.schedule(deadline: .now() + 15.0, repeating: .seconds(15), leeway: .seconds(1))
        refreshTimer?.setEventHandler { [weak self] in
            guard let strongSelf = self else {
                return
            }
            
            strongSelf.getCurrencies()
        }
        
        self.refreshTimer?.resume()
    }
    
    public func stopUpdates() {
        self.refreshTimer?.cancel()
        self.refreshTimer = nil
    }
}
