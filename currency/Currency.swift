//
//  Currency.swift
//  currency
//
//  Created by MacBook on 17.05.2018.
//  Copyright © 2018 justozz. All rights reserved.
//

import Foundation

struct CurrencyPrice: Decodable {
    let amount: Float
}

struct Currency: Decodable {
    let name: String
    let price: CurrencyPrice
    let volume: Int
}

struct CurrenciesResponse: Decodable {
    let stock: [Currency]
}
