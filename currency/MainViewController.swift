//
//  MainViewController.swift
//  currency
//
//  Created by MacBook on 17.05.2018.
//  Copyright © 2018 justozz. All rights reserved.
//

import UIKit

class MainViewController: UITableViewController, ApiCurrenciesServiceDelegate {
    fileprivate var currencies: [Currency]?

    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
        
        ApiCurrenciesService.sharedInstance.delegate = self
        
        self.refreshControl?.addTarget(self, action: #selector(MainViewController.onRefreshData(_:)), for: .valueChanged)
        self.refreshButtonPressed(self)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @objc fileprivate func onRefreshData(_ sender: Any) {
        ApiCurrenciesService.sharedInstance.getCurrencies()
    }

    @IBAction func refreshButtonPressed(_ sender: Any) {
        let height = (self.navigationController?.navigationBar.frame.size.height)! + UIApplication.shared.statusBarFrame.height
        
        self.refreshControl?.beginRefreshing()
        self.tableView.setContentOffset(CGPoint(x: 0.0, y: -height - self.refreshControl!.frame.size.height), animated: true)
        self.onRefreshData(self)
    }
    
    // MARK: - ApiCurrenciesServiceDelegate
    
    func currenciesDidUpdate(_ currencies: [Currency]?) {
        self.currencies = currencies
        
        DispatchQueue.main.async {
            self.refreshControl?.endRefreshing()
            self.tableView.reloadData()
        }
    }
    
    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return currencies?.count ?? 0
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = self.tableView.dequeueReusableCell(
            withIdentifier: "currencyCell",
            for: indexPath) as? CurrencyTableViewCell else {
                
            return UITableViewCell()
        }
        
        if let currency = self.currencies?[indexPath.row] {
            cell.setData(data: currency)
        }
        
        return cell
    }

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
